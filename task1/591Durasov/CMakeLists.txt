cmake_minimum_required(VERSION 3.0)

set(SRC_FILES
        main.cpp
        common/Application.cpp
        common/Camera.cpp
        common/Mesh.cpp
        common/ShaderProgram.cpp
        common/DebugOutput.cpp
        )

SET(HEADER_FILES
        common/main.hpp
        common/Camera.hpp
        common/Mesh.hpp
        common/ShaderProgram.hpp
        common/DebugOutput.h
        common/Common.h
        )

include_directories(common)

MAKE_OPENGL_TASK(591Durasov 1 "${SRC_FILES}")
